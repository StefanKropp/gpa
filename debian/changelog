gpa (0.10.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Re-export upstream signing key without extra signatures.
  * Set upstream metadata fields: Bug-Submit.
  * Fix field name typos in debian/copyright.

  [ Andreas Rönnquist ]
  * Upgrade to Standards-Version 4.4.1 (No changes required)
  * Fix lintian warning quoted-placeholder-in-mailcap-entry

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 23 Dec 2019 21:40:06 +0100

gpa (0.10.0-2) unstable; urgency=medium

  * Add patch to disable scaling for cardkey icon in keylist
  * Replace use of GPGME_KEYLIST_MODE_LOCATE
  * Update to Standards Version 4.4.0 (No changes required)
  * Migrate to Debian compat level 12

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 14 Aug 2019 01:12:29 +0200

gpa (0.10.0-1) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * use DEP-14 branch naming
  * d/control: add Rules-Requires-Root: no

  [ Andreas Rönnquist ]
  * New upstream version 0.10.0
  * Remove patch 0002-Add-mimetimes-to-gpa.desktop.patch, applied upstream
  * Remove patch 0003-Fix-crash-on-filename-conversion-error.patch
  * Remove 0004-Fix-listing-of-algorithm-keysize-in-the-subkey-windo.patch
  * Remove patch 0005-Fix-typo.patch
  * Now require libgpgme version 1.9.0
  * Refresh patch gnupg2.patch
  * Upgrade to Standards Version 4.2.1 (No changes required)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 17 Oct 2018 13:41:54 +0200

gpa (0.9.10-3) unstable; urgency=medium

  * wrap-and-sort -ast
  * move to debhelper 11
  * standards-version: bump to 4.1.3 (no changes needed)
  * d/control: move Vcs*: to salsa
  * point to the current upstream landing page
  * d/watch: convert to v4, use https
  * pull in upstream bugfixes
  * Spelling fix from upstream, cleaned up gpa.desktop
  * enable all hardening rules

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 06 Feb 2018 00:22:32 -0500

gpa (0.9.10-2) unstable; urgency=medium

  * Use the new standard libgpgme-dev package (Closes: #846709)

 -- Andreas Rönnquist <gusnan@debian.org>  Sat, 03 Dec 2016 10:39:59 +0100

gpa (0.9.10-1) unstable; urgency=medium

  * New upstream version 0.9.10
    - Fixed deletion of X.509 keys (Closes: #625471)
  * Add gitignore ignoring debian/files

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 22 Nov 2016 19:23:38 +0100

gpa (0.9.9-4) unstable; urgency=medium

  * allow the use of gnupg when provided by gnupg2
  * updated Standards-Version to 3.9.8 (no changes needed)
  * updated Vcs-* fields
  * updated Homepage: field

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 19 Apr 2016 17:56:46 -0400

gpa (0.9.9-3) unstable; urgency=medium

  * Remove menu file, desktop spec is enough - fixes lintian
    warning command-in-menu-file-and-desktop-file

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 11 Oct 2015 12:12:40 +0200

gpa (0.9.9-2) unstable; urgency=medium

  * Fix lintian warning desktop-mime-but-no-exec-code. This should
    be the last part of fixes for the mime information, thanks to
    A Mennucc1 (Closes: #791416)

 -- Andreas Rönnquist <gusnan@debian.org>  Tue, 15 Sep 2015 21:06:48 +0200

gpa (0.9.9-1) unstable; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 10 Sep 2015 19:43:49 +0200

gpa (0.9.7-2) unstable; urgency=medium

  * Migrate from experimental to unstable
  * Don't specify version of dependency on gnupg2 (Removing
    specification on version 2.1 that was added in experimental)

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 29 Apr 2015 15:27:01 +0200

gpa (0.9.7-1) experimental; urgency=medium

  * New Upstream Version

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 12 Dec 2014 12:11:32 +0100

gpa (0.9.6-1) experimental; urgency=medium

  * New Upstream Version
  * Remove patch enable_cardman_close.patch - applied upstream
  * Fix spelling mistake in changelog
  * Build-depend on gnupg2 2.1

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 21 Nov 2014 14:35:20 +0100

gpa (0.9.5-2) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * enable File|Close for GPA card manager.
  * removing Daniel Leidert from Uploaders at his request

  [ Andreas Rönnquist ]
  * add_desktop_file_keywords.patch:
    - Add forwarded header
  * Update to debhelper 9
  * Update to Standards Version 3.9.6 (No change required)
  * Update copyrights for Debian packaging

 -- Andreas Rönnquist <gusnan@debian.org>  Thu, 25 Sep 2014 17:03:23 +0200

gpa (0.9.5-1) unstable; urgency=medium

  [ Andreas Rönnquist ]
  * Imported Upstream version 0.9.5
  * Refresh patches
  * Update Standards-Version (No change required)
  * Fix unversioned-copyright-format-uri lintian warning
  * add_desktop_file_keywords.patch:
    - Add patch to add Keywords to the desktop file
  * Add myself to uploaders
  * Require gpg-error 1.12 and gpgme 1.5.0

 -- Andreas Rönnquist <gusnan@debian.org>  Mon, 01 Sep 2014 22:04:01 +0200

gpa (0.9.4-2) unstable; urgency=medium

  * debian/upstream/signing-key.asc: Added upstream keyring.
  * debian/watch: Added signature URL.
  * moved to git
  * added myself to uploaders
  * bumped Standards-Version to 3.9.5 (no changes needed)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 31 Aug 2014 09:52:41 -0700

gpa (0.9.4-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.4.
  * debian/copyright: Updated.
  * debian/gpa.1: Removed. Shipped by upstream too.
  * debian/gpa.manpages: Removed.
  * debian/watch: Updated.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 09 May 2013 12:26:19 +0200

gpa (0.9.3-1) experimental; urgency=low

  * New upstream release.
  * debian/control (Build-Depends): Added dh-autoreconf and autopoint.
  * debian/rules: Added autoreconf addon.
  * debian/NEWS: Added.
  * debian/patches/fix_segfault_filehandler.patch: Dropped (applied upstream).
  * debian/patches/gnupg2.patch: Added.
    - configure.ac: Check for gnupg2 binary and helpers.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 11 Aug 2012 19:16:51 +0200

gpa (0.9.2-2) experimental; urgency=low

  * debian/control (Homepage): Changed to point to gnupg.org.
    (Build-Depends): Removed unused autopoint too.
  * debian/patches/fix_segfault_filehandler.patch: Added.
    - src/fileman.c: Fix a segmentation fault in file-manager mode.
  * debian/patches/series: Added.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 08 Jul 2012 14:20:49 +0200

gpa (0.9.2-2) experimental; urgency=low

  * debian/control (Homepage): Changed to point to gnupg.org.
    (Build-Depends): Removed unused autopoint too.
  * debian/patches/fix_segfault_filehandler.patch: Added.
    - src/fileman.c: Fix a segmentation fault in file-manager mode.
  * debian/patches/series: Added.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 08 Jul 2012 14:20:49 +0200

gpa (0.9.2-1) experimental; urgency=low

  * debian/control: Adjusted for experimental. Removed DM-Upload-Allowed.
    (Build-Depends): Dropped dh-autoreconf.
    (Uploaders): Adjusted my address.
  * debian/gpa.1: Added hidden options and missing references.
  * debian/rules: Removed autoreconf addon.
  * debian/watch: Fixed tp point to ftp.gnupg.org.
  * debian/patches/448765_grammar_fixes.patch: Dropped - applied upstream.
  * debian/patches/625513_fix_expiration_date.patch: Ditto.
  * debian/patches/628305_build_with_libassuan_v2.patch: Ditto.
  * debian/patches/series: Removed.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 04 Jul 2012 21:09:28 +0200

gpa (0.9.0-4) unstable; urgency=low

  * debian/control (Homepage): Updated (closes: #696826).
  * debian/patches/628305_build_with_libassuan_v2.patch: Updated.
    - src/server.c (gpa_start_server): Call assuan_sock_init (closes: #699096).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 27 Jan 2013 16:20:46 +0100

gpa (0.9.0-3) unstable; urgency=low

  * debian/gpa.1: Added hidden options and missing references.
  * debian/patches/634930_backport_disable_x509_switch.patch: Added.
    - Added option to disable X.509. Workaround for #634930.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 13 Jan 2013 19:47:37 +0100

gpa (0.9.0-2) unstable; urgency=low

  * Acknowledge NMU (closes: #628305, #634930).
  * debian/compat: Increased debhelper compatibility level to 7.
  * debian/control: Set DM-Upload-Allowed.
    (Standards-Version): Bumped to 3.9.3.
    (Build-Depends): Increased required debhelper and autotools-dev versions.
    (Vcs-Browser): Point to real location.
  * debian/gpa.1: Fixed typos. Fixed syntax (arguments may contain files).
  * debian/gpa.dirs: Dropped (useless).
  * debian/gpa.docs: Removed NEWS (closes: #614698).
  * debian/rules: Rewritten for debhelper 7. Enabled hardening flags.
  * debian/source/format: Added for source format 3.0 (quilt).
  * debian/patches/628305_build_with_libassuan_v2.patch: Added.
    - Includes patch to fix #628305, #634930.
  * debian/patches/448765_grammar_fixes.patch: Added.
    - Fix grammar issues (closes: #448765).
  * debian/patches/625513_fix_expiration_date.patch: Added.
    - Fix bases for the month (closes: #625513).
  * debian/patches/series: Added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 23 Jun 2012 17:04:46 +0200

gpa (0.9.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: checking LIBASSUAN API version... does not match.
    want=1 got=2.":
    - apply commit a27c69e from upstream git repository at
      git://git.gnupg.org/gpa.git:
      + configure.ac: Set NEED_LIBASSUAN_VERSION and NEED_LIBASSUAN_API to
        1.1.0 and 2 resp.
      + server.c: Update to new assuan interface.
    - bump build-dependency on libassuan-dev to >= 2
    - use dh-autoreconf to regenerate autotools stuff
    (Closes: #628305, #634930)

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Feb 2012 16:05:11 +0100

gpa (0.9.0-1) unstable; urgency=low

  * New upstream release (closes: #381503).
    - Fixes several segmentation faults and loops
      (closes: #183708, #229818, #237131, #247783, #280760).
    - Working key signing (closes: #181936, #342711).
    - Do not treat keys with no expiration as expired (closes: #189102).
    - Doesn't freeze if no private key is avilable (closes: #206046, #424945).
    - Working HKP keyserver support (closes: #267275).
    - Working multi-key export (closes: #274153).
    - Typo fix in German translation (closes: #313759).
    - Ship a .desktop file (closes: #351579).
    - Fixes an FTBFS (closes: #548036).

  * debian/compat: Added with dh compat level 5.
  * debian/control: Added Homepage and Vcs* fields.
    (Maintainer): New maintainer is the GnuPG maintainers team. Thanks to
    Bastian Blank for maintaining gpa in the past.
    (Uploaders): Added myself.
    (Standards-Version): Bumped to 3.8.3.
    (Build-Depends): Completed and adjusted the requirements.
    (Depends): Added misc-depends variable. Added gpgsm.
    (Description): Improved.
  * debian/copyright: Updated.
  * debian/gpa.1: Added with thanks to Arthur de Jong (closes: #89551).
  * debian/gpa.docs: Also install AUTHORS and THANKS (closes: #225286).
  * debian/gpa.install: Added.
  * debian/gpa.manpages: Added.
  * debian/gpa.menu (section): Fixed (closes: #265000).
  * debian/rules: Rewritten in parts.
  * debian/watch: Added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 05 Dec 2009 00:04:30 +0100

gpa (0.7.0-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * move xpm icon file to /usr/share/pixmaps. Closes: #363575

 -- Andreas Barth <aba@not.so.argh.org>  Tue, 12 Dec 2006 13:43:28 +0000

gpa (0.7.0-1) unstable; urgency=low

  * New upstream version
    - now uses libgpgme11 (closes: #211919)
    - don't longer freeze on longtime operations (closes: #98327,
      #181298, #161905)

 -- Bastian Blank <waldi@debian.org>  Wed, 22 Oct 2003 18:42:01 +0200

gpa (0.6.1-2) unstable; urgency=low

  * make backups 600 (closes: #196375)
  * don't link undocumented manpage.

 -- Bastian Blank <waldi@debian.org>  Fri, 06 Jun 2003 17:29:30 +0200

gpa (0.6.1-1) unstable; urgency=low

  * New upstream version
  * fix unsigned char issue (closes: #180490)

 -- Bastian Blank <waldi@debian.org>  Fri, 14 Feb 2003 07:43:42 +0100

gpa (0.6.0-1) unstable; urgency=low

  * New upstream version (closes: #165464, #101445, #103984, #141194,
    #142529, #159287, #167087, #141045, #145283, #148679)

 -- Bastian Blank <waldi@debian.org>  Fri, 31 Jan 2003 18:45:40 +0100

gpa (0.4.3-2) unstable; urgency=low

  * add config.* update routines to debian/rules

 -- Bastian Blank <waldi@debian.org>  Sun, 31 Mar 2002 00:52:23 +0100

gpa (0.4.3-1) unstable; urgency=low

  * New upstream version (closes: #134966)
  * moved into main

 -- Bastian Blank <waldi@debian.org>  Tue, 26 Mar 2002 21:03:59 +0100

gpa (0.4.2-2) unstable; urgency=low

  * fixed color spec in icon (closes: #107576)
  * update Standards-Version to 3.5.2
  * no longer include upstream INSTALL

 -- Bastian Blank <waldi@debian.org>  Sat, 25 Aug 2001 19:47:14 +0200

gpa (0.4.2-1) unstable; urgency=low

  * New upstream version

 -- Bastian Blank <waldi@debian.org>  Sat,  7 Jul 2001 22:52:51 +0200

gpa (0.4.1-2) unstable; urgency=low

  * fix build-depends (closes: #93718)

 -- Bastian Blank <waldi@debian.org>  Thu, 12 Apr 2001 21:44:09 +0200

gpa (0.4.1-1) unstable; urgency=low

  * New upstream version
  * fix pointer problem in ./jnlib/logging.c +230 (closes: #90920)

 -- Bastian Blank <waldi@debian.org>  Thu,  5 Apr 2001 17:28:51 +0200

gpa (0.3.1-2) unstable; urgency=low

  * add icon (closes: #89678)
  * change description (closes: #89681)
  * updating build-depends

 -- Bastian Blank <waldi@debian.org>  Fri, 16 Mar 2001 18:27:36 +0100

gpa (0.3.1-1) unstable; urgency=low

  * Disable tips.
  * Fix header problem in ./jnlib/stringhelp.c
  * Initial Release. (closes: #80518)

 -- Bastian Blank <waldi@debian.org>  Mon, 12 Feb 2001 21:42:28 +0100

gpa (0.3.1-0.2) unstable; urgency=low

  * Write description.

 -- Bastian Blank <bastianb@gmx.de>  Tue, 26 Dec 2000 12:44:00 +0100

gpa (0.3.1-0.1) unstable; urgency=low

  * Initial internal Release.

 -- Bastian Blank <bastianb@gmx.de>  Mon, 11 Dec 2000 08:13:59 +0100
