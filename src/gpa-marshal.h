
#ifndef __gpa_marshal_MARSHAL_H__
#define __gpa_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* INT:STRING,STRING (/home/wk/s/gpa/src/gpa-marshal.list:1) */
extern void gpa_marshal_INT__STRING_STRING (GClosure     *closure,
                                            GValue       *return_value,
                                            guint         n_param_values,
                                            const GValue *param_values,
                                            gpointer      invocation_hint,
                                            gpointer      marshal_data);

G_END_DECLS

#endif /* __gpa_marshal_MARSHAL_H__ */

